

import Button from '../Button';
import { Container, FloatLeft, FloatRight,Loginbtn } from './styles';
import recyclingLogo from '../../assets/recycling.png'

function Header() {
  return (
    <Container>
      <FloatLeft>
        <img className="logo" src={recyclingLogo} alt="Recycling Logo" />
      </FloatLeft>
      <FloatRight>
        <Loginbtn href="http://localhost:3000/login">Entrar</Loginbtn>
    

        <Button href="http://localhost:3000/register" text='Cadastre-se'/>
    
      </FloatRight>
    </Container>
  );
};

export default Header;
