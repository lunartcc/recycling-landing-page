

import { Container } from './styles';

interface ButtonProps {
  text: string;
  href?: string;
}

function Button({text, href}: ButtonProps) {
  return (
    <Container href={href || 'http://localhost:3000/login'}>
      {text}
    </Container>
  );
};

export default Button;
