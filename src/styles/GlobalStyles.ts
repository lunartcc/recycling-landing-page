import { createGlobalStyle} from 'styled-components';


export const GlobalStyles = createGlobalStyle`
    *{
        margin:0;
        padding:0;
        box-sizing: border-box
    }

    body {
        font-family: 'Montserrat', sans-serif;
    }

    h1, h2, h3, h4, h5, h6 {
        font-family: 'Inter', sans-serif;
    }

    *,input, button {
        font-family: 'Montserrat', sans-serif;
    }

`

// export const theme = {
//     colors: {
//       primary: '#4FCD90',
//       secondary: '#4BC289',
//       tertiary: '#EBFFF4',
//       gray: '#BCBCBC',
//       red: '#FF3535',
//       white: '#FFF',
//       black: '#000',

//     },
// }
